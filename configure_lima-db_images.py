import gitlab
import os
import sys
import re

branch_slug = sys.argv[1]

limadb_slug = "LIMADB_REF_SLUG"

project_names = {
    'lima-masterdata': 'MASTERDATA_REF_SLUG',
    'lima-processinstancedata': 'PROCESSINSTANCEDATA_REF_SLUG',
    'lima-cockpit': 'COCKPIT_REF_SLUG',
    'lima-ems': 'EMS_REF_SLUG',
    'lima-routing': 'ROUTING_REF_SLUG',
    'lima-oidc': 'OIDC_REF_SLUG',
    'lima-notification': 'NOTIFICATION_REF_SLUG',
    'lima-web': 'FRONTEND_REF_SLUG',
    'lima-osbmock': 'OSBMOCK_REF_SLUG',
    'lima-admock': 'ADMOCK_REF_SLUG',
    'lima-eventlog': 'EVENTLOG_REF_SLUG',
    'lima-reporting': 'REPORTING_REF_SLUG',
    'lima-backend': 'BACKEND_REF_SLUG',
    'lima-db': limadb_slug
}


def check_if_branch_is_set_in_environment(project_name, project_slug_envvar):
    branch_name = os.environ.get(project_slug_envvar)
    if branch_name is not None:
        if project_name == "lima-db":
            return branch_name
        else:
            return re.sub(r"[^a-z\d]", "-", branch_name.lower())


def find_corresponding_branch_in_gitlab_project(project, branch_slug):
    for branch in project.branches.list(iterator=True):

        if re.sub(r"[^a-z\d]", "-", branch.attributes["name"].lower()) == branch_slug:

            if project.name == "lima-db":
                return branch.attributes["name"]
            else:
                return branch_slug


def get_environment_variables(branch_slug):
    gitlab_client = gitlab.Gitlab(private_token=os.environ.get("GITLAB_TOKEN"))

    # Create dictionary to save image slugs
    # limadb_slug is an exception: Needs to be branch name
    env_variables = {limadb_slug: 'develop'}

    for project_name, project_slug_envvar in project_names.items():
        project = gitlab_client.projects.get(f"tennet-lima/{project_name}")

        slug_value = check_if_branch_is_set_in_environment(project_name, project_slug_envvar)

        if slug_value is None:
            slug_value = find_corresponding_branch_in_gitlab_project(project, branch_slug)

        if slug_value is not None:
            env_variables[project_slug_envvar] = slug_value

    return env_variables


def get_commit_tag_env_var():
    project_name = os.environ.get("CI_PROJECT_NAME")
    env_var_name = project_names[project_name]
    commit_tag_env_var_name = re.sub(r"_REF_SLUG", r"_IMAGE_TAG", env_var_name)
    return {commit_tag_env_var_name: os.environ.get("CI_COMMIT_SHORT_SHA")}


def write_environment_file(branch_slug):
    env_variables = get_environment_variables(branch_slug)
    commit_tag_env_var = get_commit_tag_env_var()
    env_variables.update(commit_tag_env_var)

    with open("limadb.env", "w") as output:
        variables = ""

        for name, value in env_variables.items():
            value_str = f"{name}={value}"
            print(value_str)
            output.write(f"{value_str}\n")
            variables += f"{value_str};"

        output.write(f"IMAGE_REFS={variables}")


if __name__ == '__main__':
    write_environment_file(branch_slug)
